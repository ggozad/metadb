\connect metadb;

INSERT INTO artefact (artefact_type, meta) VALUES
('Sample', '{"gender": "female"}'),
('Sample', '{"gender": "male"}'),
('Sequence Read', '{"malefoo": "bar"}'),
('Sequence Read', '{"femalefoo": "bar"}'),
('Variant Calling Result', '{"foo": "bar"}');

INSERT INTO process (process_type, meta, started_on, ended_on) VALUES
('Sequencing','{"foo": "bar"}', '1/1/2023', '2/1/2023'),
('Sequencing','{"foo": "bar"}', '1/1/2023', '2/1/2023'),
('Variant Calling','{"foo": "bar"}', '2/1/2023', '3/1/2023');


INSERT INTO process_input (process_id, artefact_id) VALUES
(1, 1),
(2, 2),
(3, 3);

INSERT INTO process_output (process_id, artefact_id) VALUES
(1, 3),
(2, 4),
(3, 5);