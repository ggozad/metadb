\connect metadb;

CREATE TABLE artefact (
    id SERIAL PRIMARY KEY,
    artefact_type TEXT,
    meta JSONB,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE process (
    id SERIAL PRIMARY KEY,
    process_type TEXT,
    meta JSONB,
    started_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ended_on TIMESTAMP
);

CREATE TABLE process_input (
    process_id INTEGER REFERENCES process(id),
    artefact_id INTEGER REFERENCES artefact(id),
    PRIMARY KEY (process_id, artefact_id)
);

CREATE TABLE process_output (
    process_id INTEGER REFERENCES process(id),
    artefact_id INTEGER REFERENCES artefact(id),
    PRIMARY KEY (process_id, artefact_id)
);

create function "process_outputsByProcessId"(p process)
returns setof artefact as $$
  select artefact.*
  from artefact
  inner join process_output on (process_output.artefact_id = artefact.id)
  where process_output.process_id = p.id;
$$ language sql stable;

create function "process_inputsByProcessId"(p process)
returns setof artefact as $$
  select artefact.*
  from artefact
  inner join process_input on (process_input.artefact_id = artefact.id)
  where process_input.process_id = p.id;
$$ language sql stable;
