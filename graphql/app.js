import express from "express";
import { postgraphile } from "postgraphile";
import ConnectionFilterPlugin from "postgraphile-plugin-connection-filter";
import SimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";

const app = express();

app.use(
  postgraphile(process.env.DATABASE_URL, "public", {
    subscriptions: true,
    watchPg: true,
    dynamicJson: true,
    setofFunctionsContainNulls: false,
    ignoreRBAC: false,
    showErrorStack: "json",
    extendedErrors: ["hint", "detail", "errcode"],
    graphiql: true,
    enhanceGraphiql: true,
    enableQueryBatching: true,
    legacyRelations: "omit",
    appendPlugins: [
      SimplifyInflectorPlugin,
      ConnectionFilterPlugin,
      // PgManyToManyPlugin,
    ],
  })
);

app.listen(process.env.PORT || 5433);
